<?php
$defaults = array(
	'theme_location'  => 'main_menu_mobile',
	'container_class' => 'menu-container-mobile',
	'menu_class'      => 'flex-container-small-v big menu',
	'fallback_cb'     => false // No default menu
);

wp_nav_menu( $defaults );