 <?php get_header(); ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <h1>Prestations</h1>
  
  <div class="w80 tiny-w100 center caterer-services">
  
   <?php if (have_posts()) : ?>
   <?php while (have_posts()) : the_post(); ?>
   <?php if ('bb_caterer_service' == get_post_type($post)) : ?>
   <?php $catererService = array();
   $catererService['title'] = get_the_title();
   $catererService['price'] = get_post_meta(get_the_ID(), '_bb_caterer_service_price', true); ?>
   <?php get_template_part('content', 'bb_caterer_service'); ?>
   <?php endif; ?>
   <?php endwhile; ?>
   <?php endif; ?>
   
  </div>
  
 </section>
 
 <?php get_footer(); ?>
