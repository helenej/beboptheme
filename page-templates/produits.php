<?php
/*
 Template Name: Page produits
*/
get_header(); ?>
 <?php get_template_part('interactive-menu') ?>
 <?php get_template_part('menu') ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php get_template_part('contact') ?>
  
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article class="pal-small-pam-tiny-pas big no-title">
   <?php the_content(); ?>
  </article>
  
  <?php endwhile; ?>
  <?php endif; ?>
  
  <?php get_template_part('loop', 'produits'); ?>
 
  <!-- Menus -->
  <?php global $menus; foreach ($menus as $menu) : ?>
  
  <article class="bbMenu">
   <h1><?php echo $menu['title']; ?></h1>
   <?php get_template_part('content', 'bb_menu'); ?>
  </article>
  
  <?php endforeach; ?>
  
  <!-- Sandwiches -->
  
  <h1>Sandwiches</h1>
  
  <div class="grid-3-small-1 sandwiches">
  
   <?php foreach ($menus as $menu) : ?>
   <?php foreach ($menu['sandwiches'] as $sandwich) : ?>
   <?php get_template_part('content', 'bb_sandwich'); ?>
   <?php endforeach; ?>
   <?php endforeach; ?>
   
  </div>
  
 </section>
 
 <?php get_footer(); ?>
