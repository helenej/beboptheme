<?php
/*
 Template Name: Page traiteur
*/
get_header(); ?>
 <?php get_template_part('interactive-menu') ?>
 <?php get_template_part('menu') ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php get_template_part('contact') ?>
  
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  
  <article class="pal-small-pam-tiny-pas big no-title">
   <?php the_content(); ?>
  </article>
  
  <?php endwhile; ?>
  <?php endif; ?>
  
  <?php get_template_part('loop', 'traiteur'); ?>
  
  <!-- Caterer Services -->
  
  <h1>Prestations</h1>
  
  <div class="w80 small-w100 center caterer-services">
  
   <?php $terms = get_terms('service_type', array(
           'orderby' => 'id', // Set an id beginning with a digit to the caterer service type for ordering
           'order' => 'ASC',
           'hide_empty' => false,
           'parent' => 0)); ?>
   <?php if ($terms) : ?>
   <?php display_caterer_services_hierarchy($terms, 1); ?>
   <div class="row ptl tiny-hidden">
    <div class="col w90 txtright"><h1>Total : </h1></div>
    <div id ="total" class="col w10 big txt-green"></div>
   </div>
   <?php endif; ?>
   
  </div>
  
  <?php echo apply_filters('the_content', '[order_show_cart]'); ?> <!-- shortcode to display cart in a form for email sending -->
  
 </section>
 
 <?php get_footer(); ?>
