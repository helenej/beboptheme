��    T      �  q   \            !     /     G     Z     g     x     �  
   �     �     �  	   �     �     �                    4     I  %   _  
   �     �     �  	   �     �     �     �     �  
   �     �     
	     	     2	     ;	     H	  #   ^	     �	     �	     �	     �	     �	     �	     
     #
     )
     8
     H
     V
     e
     s
     �
     �
     �
     �
      �
     �
                     3  	   H     R     `  $   {     �     �     �     �               3  '   @     h  !   �  '   �     �      �            !   ;  #   ]     �  !   �  #   �  �  �     �  (   �     �     �            %   <     b     q     �     �     �     �     �               *     F  1   d     �     �     �     �     �          #     0     5     B     V     s     �     �  !   �  -   �     �       &   2     Y  *   n     �     �     �     �     �     �                %     F     ^     o     �  )   �     �     �     �  $        9     U     b     s     �     �     �     �     �  
   �     �     �     �       
             3     8     A     T     Z     g     {     �     �        	   K      T   =   9      7      #   <   1           C   )   *   @   O   &      ,         A           8   $       "           F          -                L             %   '   I                      2   :              H       N   E   M   R   >   .      ;         G       3                 J                   
   S   0   P           +   ?           /       4   6   5   (   !       D   Q      B                         Add New Bread Add New Caterer Service Add New Ingredient Add New Menu Add New Sandwich Add New Service Type Add or remove ingredients All Breads All Caterer Services All Ingredients All Menus All Sandwiches All Service Types Appears in the footer Bread Caterer Service Price Caterer Service Type Caterer Service Type: Choose from the most used ingredients Edit Bread Edit Caterer Service Edit Ingredient Edit Menu Edit Sandwich Edit Service Type Ingredients Menu Menu Price New Bread Name New Caterer Service New Ingredient Name New Menu New Sandwich New Service Type Name No caterer services found in Trash. No caterer services found. No ingredients found. No menus found in Trash. No menus found. No sandwiches found in Trash. No sandwiches found. Popular Ingredients Price Sandwich Bread Sandwich Bread: Sandwich Menu Sandwich Price Search Breads Search Caterer Services Search Ingredients Search Menus Search Sandwiches Search Service Types Separate ingredients with commas Service Type Update Bread Update Ingredient Update Service Type View Caterer Service View Menu View Sandwich Widgets area at the bottom add new on admin barCaterer Service add new on admin barMenu add new on admin barSandwich admin menuCaterer Services admin menuMenus admin menuSandwiches caterer serviceAdd New menuAdd New post type general nameCaterer Services post type general nameMenus post type general nameSandwiches post type singular nameCaterer Service post type singular nameMenu post type singular nameSandwich sandwichAdd New taxonomy general nameBreads taxonomy general nameIngredients taxonomy general nameService types taxonomy singular nameBread taxonomy singular nameIngredient taxonomy singular nameService type Project-Id-Version: 
POT-Creation-Date: 2016-03-03 21:04+0100
PO-Revision-Date: 2016-03-03 21:05+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 Ajouter un pain Ajouter une nouvelle prestation traiteur Ajouter un ingrédient Ajouter un nouveau menu Ajouter un nouveau sandwich Ajouter un type de prestation Ajouter ou supprimer des ingrédients Tous les pains Toutes les prestations traiteur Tous les ingrédients Tous les menus Tous les sandwiches Tous les types de prestation Apparaît dans le pied de page Pain Prix ​​du service traiteur Type de prestation traiteur Type de prestation traiteur : Choisir parmi les ingrédients les plus utilisés Modifier le pain Modifier la prestation traiteur Modifier l'ingrédient Modifier le menu Modifier le sandwich Modifier le type de prestation Ingrédients Menu Prix du menu Nom du nouveau pain Nouvelle prestation traiteur Nom du nouvel ingrédient Nouveau menu Nouveau sandwich Nom du nouveau type de prestation Pas de prestation trouvée dans la corbeille. Pas de prestation trouvée. Pas d'ingrédient trouvé. Pas de menu trouvé dans la corbeille. Pas de menu trouvé. Pas de sandwich trouvé dans la corbeille. Pas de sandwich trouvé. Ingrédients utilisés Prix Pain à sandwich Pain à sandwich : Menu du sandwich Prix du sandwich Chercher un pain Chercher une prestation traiteur Chercher un ingrédient Chercher un menu Chercher un sandwich Chercher un type de prestation Séparer les ingrédients par une virgule Type de prestation Mettre à jour le pain Mettre à jour l'ingrédient Mettre à jour le type de prestation Voir la prestation traiteur Voir le menu Voir le sandwich Zone de widgets basse Prestation traiteur Menu Sandwich Prestations traiteur Menus Sandwiches Ajouter une nouvelle Ajouter un nouveau Prestations traiteur Menus Sandwiches Prestation traiteur Menu Sandwich Ajouter un nouveau Pains Ingrédients Types de prestation Pain Ingrédient Type de prestation 