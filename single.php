 <?php get_header(); ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php get_template_part('content', 'post'); ?>
  <?php endwhile; ?>
  <?php endif; ?>
  
 </section>
 
 <?php get_footer(); ?>
