<?php

// Global variables
$menus = array();

// Hook into some actions and call some functions when they fire

// Bebop setup
add_action('after_setup_theme', 'bebop_setup');
function bebop_setup()
{
    //Load all files from the includes directory
    /*foreach (glob('../wp-content/themes/bebop/inc/*.php') as $file) {
        include $file;
    }*/
    locate_template(array('inc/custom_meta_boxes_functions.php'), true, true);
    locate_template(array('inc/custom_pt_functions.php'), true, true);
    locate_template(array('inc/custom_tax_functions.php'), true, true);
    locate_template(array('inc/scripts_functions.php'), true, true);
    locate_template(array('inc/custom_functions.php'), true, true);
    
    load_theme_textdomain('bebop', get_template_directory() . '/languages'); // Load translation
    
    add_theme_support('post-thumbnails'); // Enable thumbnails
}

// Register menu
add_action('init', 'bebop_add_menu');
function bebop_add_menu()
{
    register_nav_menu('main_menu_left', 'Main menu - left part');
    register_nav_menu('main_menu_right', 'Main menu - right part');
    register_nav_menu('main_menu_mobile', 'Main menu - mobile version');
}

// Add custom classes to menu items
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item)
{
	$classes[] = "txtcenter";
	$classes[] = "flex-item-fluid";
	return $classes;
}

add_action('init', 'create_bb_sandwich_pt');
add_action('init', 'create_bb_menu_pt');
add_action('init', 'create_bb_caterer_service_pt');
add_action('init', 'create_sandwich_tax', 0);
add_action('init', 'create_caterer_service_tax', 0);

add_action('add_meta_boxes', 'bb_sandwich_add_price_meta_box');
add_action('add_meta_boxes', 'bb_sandwich_add_menu_meta_box');
add_action('add_meta_boxes', 'bb_menu_add_meta_box');
add_action('add_meta_boxes', 'bb_caterer_service_add_meta_box');

add_action('save_post', 'bb_sandwich_save_price_meta_box_data');
add_action('save_post', 'bb_sandwich_save_menu_meta_box_data');
add_action('save_post', 'bb_menu_save_meta_box_data');
add_action('save_post', 'bb_caterer_service_save_meta_box_data');

add_action('wp_enqueue_scripts', 'bebop_scripts');

/* Register sidebars */
add_action('widgets_init','bebop_add_sidebars');
function bebop_add_sidebars()
{
    register_sidebar(array(
    'id' => 'widget_area_bottom',
    'name' => __('Widgets area at the bottom', 'bebop'),
    'description' => __('Appears in the footer', 'bebop'),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
            ));
}
