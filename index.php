 <?php get_header(); ?>
 <?php get_template_part('interactive-menu') ?>
 <?php get_template_part('menu') ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php get_template_part('contact') ?>
  
  <article id="presentation" class="pal-small-pam-tiny-pas big">
   <h1>Qui sommes-nous ?</h1>
   <?php $firstPage = get_page_by_title('accueil'); ?>
   <?php if ($firstPage) : echo $firstPage->post_content; ?>
   <?php endif; ?>
  </article>
  
 </section>
 
 <?php get_template_part('map') ?>
 
 <?php get_footer(); ?>
