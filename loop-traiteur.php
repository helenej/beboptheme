<?php
$args = array(
        'post_type'         => array('bb_caterer_service'),
        'posts_per_page'    => -1, // Show all posts (default is 10)
        'orderby' => 'title',
        'order' => 'ASC'
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    global $catererServices;
    $catererServices = array();
    while ($query->have_posts()) {
        $query->the_post();
        if ('bb_caterer_service' == get_post_type($post)) {
            $catererServiceId = get_the_ID();
            $serviceType = get_the_terms($catererServiceId, 'service_type');
            if (!$serviceType) {
                $serviceType = array();
            }
            $serviceTypeId = reset($serviceType)->term_id; // First item is the service type
            $price = get_post_meta($catererServiceId, '_bb_caterer_service_price', true);
            if (!$price) {
                $price = 0;
            }
            
            if (!array_key_exists($serviceTypeId, $catererServices)) {
                $catererServices[$serviceTypeId] = array();
            }
            
            $catererServices[$serviceTypeId][$catererServiceId] = array(
                    'title' => get_the_title(),
                    'price' => $price
            );
        }
    }
}
wp_reset_postdata(); // Restore original Post Data