<?php
$args = array(
        'post_type'         => array('bb_sandwich'),
        'posts_per_page'    => -1, // Show all posts (default is 10)
        'orderby'           => 'meta_value_num', // Order by price
        'meta_key'          => '_bb_sandwich_price',
        'order'             => 'ASC'
);
$query = new WP_Query($args);
if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        if ('bb_sandwich' == get_post_type($post)) {
            $sandwichId = get_the_ID();
            $sandwichMenuId = get_post_meta($sandwichId, '_bb_sandwich_menu', true);
            
            // Handle bread and initialize name and slug if not defined
            $bread = get_the_terms($sandwichId, 'bread');
            if ($bread) {
                $breadName = reset($bread)->name; // First item is the bread
                $breadSlug = reset($bread)->slug;
            } else {
                $breadName = "";
                $breadSlug = "";
            }
            
            // Handle price and initialize if not defined
            $price = get_post_meta($sandwichId, '_bb_sandwich_price', true);
            if (!$price) {
                $price = 0;
            }
            
            // Fill sandwich
            $sandwich = array(
                    'title' => get_the_title(),
                    'breadName' => $breadName,
                    'breadSlug' => $breadSlug,
                    'ingredients' => strip_tags(get_the_term_list($sandwichId, 'ingredient', '', ', ')),
                    'price' => $price,
                    'idMenu' => $sandwichMenuId
            );
            
            global $menus;
            if (array_key_exists($sandwichMenuId, $menus)) {
                // Replace existing sandiwh in menu
                $menus[$sandwichMenuId]['sandwiches'][$sandwichId] = $sandwich;
            } else {
                $price = get_post_meta($sandwichMenuId, '_bb_menu_price', true);
                if (!$price) {
                    $price = 0;
                }
                
                // Fill menus with new sandwich
                $menus[$sandwichMenuId] = array(
                    'title' => get_the_title($sandwichMenuId),
                    'price' => $price,
                    'sandwiches' => array($sandwichId => $sandwich)
                );
            }
        }
        
        // Pass PHP data to script for menu
        wp_localize_script('interactive-menu', 'menuParams', $menus);
    }
}
wp_reset_postdata(); // Restore original Post Data