<?php
$defaults = array(
	'theme_location'  => 'main_menu_left',
	'container_class' => 'menu-container-left',
	'menu_class'      => 'flex-container big menu',
	'fallback_cb'     => false // No default menu
);

wp_nav_menu( $defaults );