<?php global $menu; ?>
<div class="grid-7-small-3 txt-light-grey">
 <div>
 
  <!-- Sandwich list -->
  <ul>
  <?php
  $sandwichCounter = 1;
  foreach ($menu['sandwiches'] as $sandwich) :
  ?>
  <li>
   <p class="txt-green"><?php echo $sandwich['title']; ?></p>
   <?php if (count($menu['sandwiches']) > $sandwichCounter) : ?><span class="list-separator">ou</span><?php endif; ?>
  </li>
  <?php
  $sandwichCounter++;
  endforeach;
  ?>
  </ul>
 </div>
 
 <div><span class="menu-separator">+</span></div>
 <div class="big"><img src="<?php bloginfo('template_url'); ?>/images/drink.png" alt="" /><br />1 boisson</div>
 <div><span class="menu-separator">+</span></div>
 <div class="big"><img src="<?php bloginfo('template_url'); ?>/images/sweet.png" alt="" /><br />1 dessert</div>
 <div><span class="menu-separator">=</span></div>
 <div class="price">
  <?php 
  $intPart = 0;
  $decimalPart = 0;
  if ($menu['price']) {
   list($intPart, $decimalPart) = preg_split('/[\.,]/', $menu['price']);
  }
  ?>
  <div><?php echo $intPart; ?></div>
  <div><span>,<?php echo $decimalPart; if ((strlen($intPart) == 1) && (strlen($decimalPart) == 1)) : echo '0'; endif; ?></span><br /><span>€</span></div>
 </div>
 
</div>
