<?php global $catererService; ?>
<div class="row pts big">
 <div class="col w70 small-w40 txtleft"><?php echo $catererService['title']; ?></div>
 <div class="col w10 small-w20 pls txtright txt-light-grey"><?php echo number_format_i18n($catererService['price'], 2); ?> €</div>
 <div class="col w10 small-w20 pls tiny-hidden"><?php echo apply_filters('the_content', '[order_add_item price="' . $catererService['price'] .'"]' . $catererService['title'] . '[/order_add_item]'); ?></div> <!-- shortcode to add item input with price as attribute and title as content -->
 <div class="col w10 small-w20 pls txt-green tiny-hidden"><span class="total-row"></span></div>
</div>
