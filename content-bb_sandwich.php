<?php global $sandwich; ?>
<div>
 <h2>
  <?php echo $sandwich['title']; ?>
 </h2>
 <div class="grid-2">
  <div>
   <?php if ($sandwich['breadSlug']) : ?>
   <img src="<?php bloginfo('template_url'); ?>/images/<?php echo $sandwich['breadSlug']; ?>.png" alt="<?php echo $sandwich['breadName']; ?>" title="<?php echo $sandwich['breadName']; ?>" />
   <?php endif; ?>
  </div>
  <div class="txt-light-grey">
   <p><span class="txt-green">Ingrédients : </span><?php echo $sandwich['ingredients']; ?></p>
   <p><span class="txt-green">Prix seul : </span><?php echo number_format_i18n($sandwich['price'], 2); ?>€</p>
  </div>
 </div>
</div>
