<?php

/**
 * Display the caterer service type hierarchy and the caterer services associated.
 * @param array $terms an array of service types of the same level
 * @param int $i the level of the hierarchy (for the heading)
 */
function display_caterer_services_hierarchy($terms, $i) {
    foreach ($terms as $term) {
    	
    	// Build row
    	
    	$class = ' class="';
    	if ($i == 2) {
    		$class .= ' txt-green';
    	} elseif ($i == 3) {
    		$class .= ' big txt-light-grey';
    	}
    	$class .= '"';
    	
        $heading = '<div class="row"><div class="col txtleft"><h' . $i . $class . '>' . $term->name . '</h' . $i . '>';
        
        $description = $term->description;
        if ($description) {
            $heading .= ' <span class="pls txt-light-grey">' . $term->description . '</span>';
        }
        
        $heading .= '</div></div>';
        
        //--
        
        echo $heading;
        
        $termChildren = get_terms('service_type', array(
                'orderby' => 'id', // Set an id beginning with a digit to the caterer service type for ordering
                'order' => 'ASC',
                'hide_empty' => false,
                'parent' => $term->term_id));
        
        global $catererServices;
        if (($i > 1 || empty($termChildren)) && $catererServices[$term->term_id]) { // When $i = 1 it means that it is the first parent but it still can be the last parent (empty($termChildren))
            foreach ($catererServices[$term->term_id] as $tmpCatererService) {
                global $catererService;
                $catererService = $tmpCatererService;
                get_template_part('content', 'bb_caterer_service');
            }
        }
        
        if ($termChildren) {
            // Call this function recursively
            display_caterer_services_hierarchy($termChildren, $i + 1);
        }
    }
}
