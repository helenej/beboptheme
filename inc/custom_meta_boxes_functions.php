<?php

/**
 * Add a box for the price to the main column on the Sandwich edit screen.
 */
function bb_sandwich_add_price_meta_box() {

    $screens = array('bb_sandwich');

    foreach ($screens as $screen) {

        add_meta_box(
        'bb_sandwich_price',
        __('Price', 'bebop'),
        'bb_sandwich_price_meta_box_callback',
        $screen
        );
    }
}

/**
 * Add a box for the menu to the main column on the Sandwich edit screen.
 */
function bb_sandwich_add_menu_meta_box() {

    $screens = array('bb_sandwich');

    foreach ($screens as $screen) {

        add_meta_box(
        'bb_sandwich_menu',
        __('Menu', 'bebop'),
        'bb_sandwich_menu_meta_box_callback',
        $screen
        );
    }
}

/**
 * Add a box for the price to the main column on the Menu edit screen.
 */
function bb_menu_add_meta_box() {

    $screens = array('bb_menu');

    foreach ($screens as $screen) {

        add_meta_box(
        'bb_menu_price',
        __('Price', 'bebop'),
        'bb_menu_meta_box_callback',
        $screen
        );
    }
}

/**
 * Add a box for the price to the main column on the Caterer Service edit screen.
 */
function bb_caterer_service_add_meta_box() {

    $screens = array('bb_caterer_service');

    foreach ($screens as $screen) {

        add_meta_box(
        'bb_caterer_service_price',
        __('Price', 'bebop'),
        'bb_caterer_service_meta_box_callback',
        $screen
        );
    }
}

/**
 * Print the box content for the sandwich price.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function bb_sandwich_price_meta_box_callback($post) {

    // Add an nonce field so we can check for it later
    wp_nonce_field('bb_sandwich_price_meta_box', 'bb_sandwich_price_meta_box_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form
    */
    $value = get_post_meta($post->ID, '_bb_sandwich_price', true);

    echo '<label for="price">';
    _e('Sandwich Price', 'bebop');
    echo '</label> ';
    echo '<input type="number" step="0.05" min="0" id="price" name="price" value="' . esc_attr($value) . '" size="25" /> €';
}

/**
 * Prints the box content for the menu of the sandwich.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function bb_sandwich_menu_meta_box_callback($post) {

    // Add an nonce field so we can check for it later
    wp_nonce_field('bb_sandwich_menu_meta_box', 'bb_sandwich_menu_meta_box_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form
    */
    $value = get_post_meta($post->ID, '_bb_sandwich_menu', true);

    echo '<label for="menu">';
    _e('Sandwich Menu', 'bebop');
    echo '</label> ';
    echo '<select name="menu">';

    $args = array('post_type' => 'bb_menu');
    $loop = new WP_Query($args);
    while ($loop->have_posts()) : $loop->the_post();
    $id = get_the_ID();
    echo '<option value="' . $id . '"';
    if ($id == $value) {
        echo ' selected';
    }
    echo '>';
    the_title();
    echo '</option>';
    endwhile;

    echo '</select';
}

/**
 * Prints the box content for the menu price.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function bb_menu_meta_box_callback($post) {

    // Add an nonce field so we can check for it later
    wp_nonce_field('bb_menu_meta_box', 'bb_menu_meta_box_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form
    */
    $value = get_post_meta($post->ID, '_bb_menu_price', true);

    echo '<label for="price">';
    _e('Menu Price', 'bebop');
    echo '</label> ';
    echo '<input type="number" step="0.05" min="0" id="price" name="price" value="' . esc_attr($value) . '" size="25" /> €';
}

/**
 * Prints the box content for the caterer service price.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function bb_caterer_service_meta_box_callback($post) {

    // Add an nonce field so we can check for it later
    wp_nonce_field('bb_caterer_service_meta_box', 'bb_caterer_service_meta_box_nonce');

    /*
     * Use get_post_meta() to retrieve an existing value
    * from the database and use the value for the form
    */
    $value = get_post_meta($post->ID, '_bb_caterer_service_price', true);

    echo '<label for="price">';
    _e('Caterer Service Price', 'bebop');
    echo '</label> ';
    echo '<input type="number" step="0.05" min="0" id="price" name="price" value="' . esc_attr($value) . '" size="25" /> €';
}

/**
 * When the sandwich is saved, save the price.
 *
 * @param int $post_id The ID of the post being saved.
 */
function bb_sandwich_save_price_meta_box_data($post_id) {
    save_custom_data($post_id, 'bb_sandwich_price_meta_box_nonce', 'bb_sandwich_price_meta_box', 'price', '_bb_sandwich_price');
}

/**
 * When the sandwich is saved, save its menu.
 *
 * @param int $post_id The ID of the post being saved.
 */
function bb_sandwich_save_menu_meta_box_data($post_id) {
    save_custom_data($post_id, 'bb_sandwich_menu_meta_box_nonce', 'bb_sandwich_menu_meta_box', 'menu', '_bb_sandwich_menu');
}

/**
 * When the menu is saved, save the price.
 *
 * @param int $post_id The ID of the post being saved.
 */
function bb_menu_save_meta_box_data($post_id) {
    save_custom_data($post_id, 'bb_menu_meta_box_nonce', 'bb_menu_meta_box', 'price', '_bb_menu_price');
}

/**
 * When the caterer service is saved, save the price.
 *
 * @param int $post_id The ID of the post being saved.
 */
function bb_caterer_service_save_meta_box_data($post_id) {
    save_custom_data($post_id, 'bb_caterer_service_meta_box_nonce', 'bb_caterer_service_meta_box', 'price', '_bb_caterer_service_price');
}

function save_custom_data($post_id, $nonce, $action, $meta_name, $meta_key) {
    /*
     * We need to verify this came from our screen and with proper authorization,
    * because the save_post action can be triggered at other times
    */

    // Check if our nonce is set
    if (! isset($_POST[$nonce])) {
        return;
    }

    // Verify that the nonce is valid
    if (! wp_verify_nonce($_POST[$nonce], $action)) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Check the user's permissions
    if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {

        if (! current_user_can('edit_page', $post_id)) {
            return;
        }

    } else {

        if (! current_user_can('edit_post', $post_id)) {
            return;
        }
    }

    /* OK, its safe for us to save the data now */

    // Make sure that it is set
    if (! isset($_POST[$meta_name])) {
        return;
    }

    // Sanitize user input
    $my_data = sanitize_text_field($_POST[$meta_name]);

    // Update the meta field in the database
    update_post_meta($post_id, $meta_key, $my_data);
}
