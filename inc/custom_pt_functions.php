<?php

/**
 * Register the custom post type bb_sandwich.
 */
function create_bb_sandwich_pt() {
    $labels = array(
            'name'               => _x('Sandwiches', 'post type general name', 'bebop'),
            'singular_name'      => _x('Sandwich', 'post type singular name', 'bebop'),
            'menu_name'          => _x('Sandwiches', 'admin menu', 'bebop'),
            'name_admin_bar'     => _x('Sandwich', 'add new on admin bar', 'bebop'),
            'add_new'            => _x('Add New', 'sandwich', 'bebop'),
            'add_new_item'       => __('Add New Sandwich', 'bebop'),
            'new_item'           => __('New Sandwich', 'bebop'),
            'edit_item'          => __('Edit Sandwich', 'bebop'),
            'view_item'          => __('View Sandwich', 'bebop'),
            'all_items'          => __('All Sandwiches', 'bebop'),
            'search_items'       => __('Search Sandwiches', 'bebop'),
            'not_found'          => __('No sandwiches found.', 'bebop'),
            'not_found_in_trash' => __('No sandwiches found in Trash.', 'bebop')
    );

    $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'sandwich'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title', 'thumbnail'),
            'menu_position'        => 5
    );

    register_post_type('bb_sandwich', $args);
}

/**
 * Register the custom post type bb_menu.
 */
function create_bb_menu_pt() {
    $labels = array(
            'name'               => _x('Menus', 'post type general name', 'bebop'),
            'singular_name'      => _x('Menu', 'post type singular name', 'bebop'),
            'menu_name'          => _x('Menus', 'admin menu', 'bebop'),
            'name_admin_bar'     => _x('Menu', 'add new on admin bar', 'bebop'),
            'add_new'            => _x('Add New', 'menu', 'bebop'),
            'add_new_item'       => __('Add New Menu', 'bebop'),
            'new_item'           => __('New Menu', 'bebop'),
            'edit_item'          => __('Edit Menu', 'bebop'),
            'view_item'          => __('View Menu', 'bebop'),
            'all_items'          => __('All Menus', 'bebop'),
            'search_items'       => __('Search Menus', 'bebop'),
            'not_found'          => __('No menus found.', 'bebop'),
            'not_found_in_trash' => __('No menus found in Trash.', 'bebop')
    );

    $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'menu'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title', 'thumbnail'),
            'menu_position'        => 5
    );

    register_post_type('bb_menu', $args);
}

/**
 * Register the custom post type bb_caterer_service.
 */
function create_bb_caterer_service_pt() {
    $labels = array(
            'name'               => _x('Caterer Services', 'post type general name', 'bebop'),
            'singular_name'      => _x('Caterer Service', 'post type singular name', 'bebop'),
            'menu_name'          => _x('Caterer Services', 'admin menu', 'bebop'),
            'name_admin_bar'     => _x('Caterer Service', 'add new on admin bar', 'bebop'),
            'add_new'            => _x('Add New', 'caterer service', 'bebop'),
            'add_new_item'       => __('Add New Caterer Service', 'bebop'),
            'new_item'           => __('New Caterer Service', 'bebop'),
            'edit_item'          => __('Edit Caterer Service', 'bebop'),
            'view_item'          => __('View Caterer Service', 'bebop'),
            'all_items'          => __('All Caterer Services', 'bebop'),
            'search_items'       => __('Search Caterer Services', 'bebop'),
            'not_found'          => __('No caterer services found.', 'bebop'),
            'not_found_in_trash' => __('No caterer services found in Trash.', 'bebop')
    );

    $args = array(
            'labels'             => $labels,
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array('slug' => 'caterer_service'),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array('title', 'thumbnail'),
            'menu_position'        => 5
    );

    register_post_type('bb_caterer_service', $args);
}
