<?php

/**
 * Load JS libraries and scripts.
 */
function bebop_scripts() {

    $extension = '.js';
    if (!SCRIPT_DEBUG) {
        $extension = '.min.js';
    }

    wp_register_script('svg-min', get_stylesheet_directory_uri() . '/js/svg' . $extension, array(), '1.0', true);

    wp_register_script('sophien', get_stylesheet_directory_uri() . '/js/sophien' . $extension, array(), '1.0', true);

    wp_register_script('utilities', get_stylesheet_directory_uri() . '/js/utilities.js', array(), '1.0', true);

    // Enqueue a script that has both svg-min, sophien
    // and utilities (registered earlier) as dependencies
    // TODO only if touch screen device?
    wp_enqueue_script('interactive-menu', get_stylesheet_directory_uri() . '/js/interactive-menu.js', array('svg-min', 'sophien', 'utilities'), '1.0', true);

    // Enqueue a script that has jquery and some jQuery UI components as dependency
    wp_enqueue_script('responsive-menu', get_stylesheet_directory_uri() . '/js/responsive-menu.js', array('jquery', 'jquery-ui-accordion'), '1.0', true);

    // Enqueue a script that has jquery and utilities as dependency
    wp_enqueue_script('traiteur', get_stylesheet_directory_uri() . '/js/traiteur.js', array('jquery', 'utilities'), '1.0', true);

    wp_enqueue_script('analytics', get_stylesheet_directory_uri() . '/js/analytics.js', array(), '1.0', false);

}
