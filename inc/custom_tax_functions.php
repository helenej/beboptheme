<?php

/**
 * Create two taxonomies, bread and ingredients, for the custom post type bb_sandwich.
 */
function create_sandwich_tax() {
    // Add new taxonomy (bread), make it hierarchical (like categories)
    $labels = array(
            'name'              => _x('Breads', 'taxonomy general name', 'bebop'),
            'singular_name'     => _x('Bread', 'taxonomy singular name', 'bebop'),
            'search_items'      => __('Search Breads', 'bebop'),
            'all_items'         => __('All Breads', 'bebop'),
            'parent_item'       => __('Sandwich Bread', 'bebop'),
            'parent_item_colon' => __('Sandwich Bread:', 'bebop'),
            'edit_item'         => __('Edit Bread', 'bebop'),
            'update_item'       => __('Update Bread', 'bebop'),
            'add_new_item'      => __('Add New Bread', 'bebop'),
            'new_item_name'     => __('New Bread Name', 'bebop'),
            'menu_name'         => __('Bread', 'bebop'),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array('slug' => 'bread'),
    );

    register_taxonomy('bread', 'bb_sandwich', $args);

    // Add new taxonomy (ingredients), NOT hierarchical (like tags)
    $labels = array(
            'name'                       => _x('Ingredients', 'taxonomy general name', 'bebop'),
            'singular_name'              => _x('Ingredient', 'taxonomy singular name', 'bebop'),
            'search_items'               => __('Search Ingredients', 'bebop'),
            'popular_items'              => __('Popular Ingredients', 'bebop'),
            'all_items'                  => __('All Ingredients', 'bebop'),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __('Edit Ingredient', 'bebop'),
            'update_item'                => __('Update Ingredient', 'bebop'),
            'add_new_item'               => __('Add New Ingredient', 'bebop'),
            'new_item_name'              => __('New Ingredient Name', 'bebop'),
            'separate_items_with_commas' => __('Separate ingredients with commas', 'bebop'),
            'add_or_remove_items'        => __('Add or remove ingredients', 'bebop'),
            'choose_from_most_used'      => __('Choose from the most used ingredients', 'bebop'),
            'not_found'                  => __('No ingredients found.', 'bebop'),
            'menu_name'                  => __('Ingredients', 'bebop'),
    );

    $args = array(
            'hierarchical'          => false,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => array('slug' => 'ingredient'),
    );

    register_taxonomy('ingredient', 'bb_sandwich', $args);
}

/**
 * Create one taxonomies, service_type, for the custom post type bb_caterer_service.
 */
function create_caterer_service_tax() {
    // Add new taxonomy (service type), make it hierarchical (like categories)
    $labels = array(
            'name'              => _x('Service types', 'taxonomy general name', 'bebop'),
            'singular_name'     => _x('Service type', 'taxonomy singular name', 'bebop'),
            'search_items'      => __('Search Service Types', 'bebop'),
            'all_items'         => __('All Service Types', 'bebop'),
            'parent_item'       => __('Caterer Service Type', 'bebop'),
            'parent_item_colon' => __('Caterer Service Type:', 'bebop'),
            'edit_item'         => __('Edit Service Type', 'bebop'),
            'update_item'       => __('Update Service Type', 'bebop'),
            'add_new_item'      => __('Add New Service Type', 'bebop'),
            'new_item_name'     => __('New Service Type Name', 'bebop'),
            'menu_name'         => __('Service Type', 'bebop'),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array('slug' => 'service_type'),
    );

    register_taxonomy('service_type', 'bb_caterer_service', $args);
}
