 <?php get_header(); ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <h1>Sandwiches</h1>
  
  <div class="grid-3-small-1 sandwiches">
  
   <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
   <?php if ('bb_sandwich' == get_post_type($post)) : ?>
   <?php $sandwich = array();
   $sandwich['ingredients'] = strip_tags(get_the_term_list(get_the_ID(), 'ingredient', '', ', '));
   $sandwich['price'] = get_post_meta(get_the_ID(), '_bb_sandwich_price', true);
   $sandwich['title'] = get_the_title();
   $bread = get_the_terms(get_the_ID(), 'bread');
   $sandwich['breadName'] = reset($bread)->name;
   $sandwich['breadSlug'] = reset($bread)->slug; ?>
   <?php get_template_part('content', 'bb_sandwich'); ?>
   <?php endif; ?>
   <?php endwhile; ?>
   <?php endif; ?>
   
  </div>
  
 </section>
 
 <?php get_footer(); ?>
