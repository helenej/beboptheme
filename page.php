 <?php get_header(); ?>
 <?php get_template_part('interactive-menu') ?>
 <?php get_template_part('menu') ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php get_template_part('contact') ?>
  
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php get_template_part('content', 'page'); ?>
  <?php endwhile; ?>
  <?php endif; ?>
  
 </section>
 
 <?php get_footer(); ?>
