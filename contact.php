  <div id="contact" class="pal grid-2-tiny-1 big">
   <aside class="txtleft">
    <p>
     <img src="<?php bloginfo('template_url'); ?>/images/icon_phone_small.png" alt="Téléphone" />
     <a href="tel:0561123206" class="mls">05 61 12 32 06</a>
    </p>
    <p>
     <img src="<?php bloginfo('template_url'); ?>/images/icon_mail_small.png" alt="E-mail" />
     <a href="mailto:bebop.toulouse@gmail.com?Subject=Site%20web" target="_top" class="mls">bebop.toulouse<wbr>@gmail.com</a>
    </p>
    <p>
     <img src="<?php bloginfo('template_url'); ?>/images/icon_facebook_small.png" alt="Facebook" />
     <a href="http://www.facebook.com/bebopcafe" target="_blank" class="mls">www.facebook.com<wbr>/bebopcafe</a>
    </p>
   </aside>
   <aside class="txtright">
    <p>
     45, rue de Metz, Toulouse<img src="<?php bloginfo('template_url'); ?>/images/icon_marker_small.png" alt="Adresse" class="mls" />
    </p>
    <p>
     Lun. - Ven. : 7h30 - 19h30<img src="<?php bloginfo('template_url'); ?>/images/icon_clock_small.png" alt="Horaire" class="mls" />
    </p>
    <p>Tous nos produits sont frais et fait maison.</p>
   </aside>
  </div>
  