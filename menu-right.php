<?php
$defaults = array(
	'theme_location'  => 'main_menu_right',
	'container_class' => 'push menu-container-right',
	'menu_class'      => 'flex-container big menu',
	'fallback_cb'     => false // No default menu
);

wp_nav_menu( $defaults );