 <?php get_header(); ?>
 
 <section class="mal-small-mam-tiny-mas pal-small-pam-tiny-pas txtcenter">
 
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
  <?php if ('bb_menu' == get_post_type($post)) : ?>
  
  <article class="bbMenu">
   <h1><?php the_title(); ?></h1>
   <?php $menu = array();
   $args = array(
           'post_type' => 'bb_sandwich',
           'posts_per_page' => -1,
           'meta_query' => array(
                   array(
                           'key' => '_bb_sandwich_menu',
                           'value' => get_the_ID(),
                   )
           )
   );
   $sandwiches = get_posts($args);
   $menu['sandwiches'] = array();
   foreach ($sandwiches as $sandwich) {
    $menu['sandwiches'][] = array('title' => get_the_title($sandwich));
   }
   $menu['price'] = get_post_meta(get_the_ID(), '_bb_menu_price', true); ?>
   <?php get_template_part('content', 'bb_menu'); ?>
  </article>
  
  <?php endif; ?>
  <?php endwhile; ?>
  <?php endif; ?>
  
 </section>
 
 <?php get_footer(); ?>
