(function() {
	if (typeof menuParams !== 'undefined') {
    
	    function menuClickHandler(price, sandwiches) {
	    	return function() {
	    		var element = document.getElementsByClassName('svg-container')[0];
	            // Clear menu
	            if (element.firstChild) {
	            	element.removeChild(element.firstChild);
	            }
	            
	            // Create array of sandwich titles
	            var sandwichTitles = [];
	            for (var i = 0; i < sandwiches.length; i++) {
	            	sandwichTitles.push(sandwiches[i]['title']);
	            }
	            
	            // Create outer menu (sandwiches)
	            var sandwichItems = sophien.createCircularMenu(element, 250, 250, 250, 70, 223, sandwichTitles);
	        	for (var i = 0; i < sandwichItems.length; i++) {
	        		sandwichItems[i].addEventListener('click', sandwichClickHandler(sandwiches[i]));
	        	}
	
	        	// Hide logo
	        	document.getElementById("logo").style.display = "none";
	
	        	createMenuContent(price);
	    	};
	    }
	
	    function sandwichClickHandler(sandwich) {
	        return function() {
	        	createSandwichContent(sandwich);
	        };
	    }
	
	    function createMenuContent(price) {
	    	// Clear content
	    	var oldContent = document.getElementById("dynamic_content");
	    	var container = document.getElementById("dynamic_container");
	    	if (oldContent) {
	        	container.removeChild(oldContent);
	    	}
	    	
	    	// Change content
	    	var contentDiv = document.createElement("div");
	    	contentDiv.id = "dynamic_content";
	
	    	var list = document.createElement("ul");
	    	var item = document.createElement("li");
	    	var itemContent = document.createElement("p");
	    	itemContent.style.color = "#B4DC32";
	    	var itemText = document.createTextNode("1 sandwich");
	    	itemContent.appendChild(itemText);
	    	item.appendChild(itemContent);
	    	var itemSeparator = document.createElement("span");
	    	itemSeparator.className = "list-separator";
	    	itemText = document.createTextNode("et");
	    	itemSeparator.appendChild(itemText);
	    	item.appendChild(itemSeparator);
	    	list.appendChild(item);
	
	    	item = document.createElement("li");
	    	itemContent = document.createElement("p");
	    	itemContent.style.color = "#B4DC32";
	    	itemText = document.createTextNode("1 boisson");
	    	itemContent.appendChild(itemText);
	    	item.appendChild(itemContent);
	    	itemSeparator = document.createElement("span");
	    	itemSeparator.className = "list-separator";
	    	itemText = document.createTextNode("et");
	    	itemSeparator.appendChild(itemText);
	    	item.appendChild(itemSeparator);
	    	list.appendChild(item);
	
	    	item = document.createElement("li");
	    	itemContent = document.createElement("p");
	    	itemContent.style.color = "#B4DC32";
	    	itemText = document.createTextNode("1 dessert");
	    	itemContent.appendChild(itemText);
	    	item.appendChild(itemContent);
	    	itemSeparator = document.createElement("span");
	    	itemSeparator.className = "list-separator";
	    	itemText = document.createTextNode("pour");
	    	itemSeparator.appendChild(itemText);
	    	item.appendChild(itemSeparator);
	    	list.appendChild(item);
	    	
	    	contentDiv.appendChild(list);
	
	    	var priceDiv = document.createElement("div");
	    	priceDiv.className = "price";
	    	
	    	var priceInnerDiv = document.createElement("div");
	    	
	    	priceParts = ['0', '00'];
	    	if (price > 0) {
	    		priceParts = price.split(/[\.,]/);
	    		if ((priceParts[0].length == 1) && (priceParts[1].length == 1)) {
		    		priceParts[1] += '0';
		    	}
	    	}
	    	
	    	var priceText = document.createTextNode(priceParts[0]);
	    	priceInnerDiv.appendChild(priceText);
	    	priceDiv.appendChild(priceInnerDiv);
	
	    	priceInnerDiv = document.createElement("div");
	    	var priceSpan = document.createElement("span");
	    	priceText = document.createTextNode("," + priceParts[1]);
	    	priceSpan.appendChild(priceText);
	    	priceInnerDiv.appendChild(priceSpan);
	    	priceInnerDiv.appendChild(document.createElement("br"));
	
	    	priceSpan = document.createElement("span");
	    	priceText = document.createTextNode("€");
	    	priceSpan.appendChild(priceText);
	    	priceInnerDiv.appendChild(priceSpan);
	
	    	priceDiv.appendChild(priceInnerDiv);
	    	contentDiv.appendChild(priceDiv);
	    	
	    	container.appendChild(contentDiv);
	    }
	
	    function createSandwichContent(sandwich) {
	    	// Clear content
	    	var oldContent = document.getElementById("dynamic_content");
	    	var container = document.getElementById("dynamic_container");
	    	if (oldContent) {
	        	container.removeChild(oldContent);
	    	}
	    	
	    	// Change content
	    	var contentDiv = document.createElement("div");
	    	contentDiv.id = "dynamic_content";
	
	    	var title = document.createElement("h3");
	    	var text = document.createTextNode("Pain : ");
	    	title.appendChild(text);
	    	contentDiv.appendChild(title);
	    	
	    	text = document.createTextNode(sandwich['breadName']);
	    	contentDiv.appendChild(text);
	    	contentDiv.appendChild(document.createElement("br"));
	
	    	title = document.createElement("h3");
	    	text = document.createTextNode("Ingrédients : ");
	    	title.appendChild(text);
	    	contentDiv.appendChild(title);
	    	
	    	var p = document.createElement("p");
	    	text = document.createTextNode(sandwich['ingredients']);
	    	p.appendChild(text);
	    	contentDiv.appendChild(p);
	
	    	title = document.createElement("h3");
	    	text = document.createTextNode("Prix seul : ");
	    	title.appendChild(text);
	    	contentDiv.appendChild(title);
	    	
	    	text = document.createTextNode(Utilities.formatPrice(Utilities.parsePrice(sandwich['price'], "€"), "€"));
	    	contentDiv.appendChild(text);
	
	    	container.appendChild(contentDiv);
	    }
	    //}
		
		/**
		 * Tmp function
		 */
		function showOrHideInteractiveMenu() {
			var outerMenu = document.getElementsByClassName('svg-container')[0];
			var innerMenu = document.getElementsByClassName('svg-container')[1];
			var content = document.getElementById("dynamic_content");
			var logo = document.getElementById("logo");
			if (window.innerWidth <= 1040) {
				outerMenu.style.visibility = 'hidden';
				innerMenu.style.visibility = 'hidden';
				if (content) {
					content.style.display = 'none';
					logo.style.display = 'inline-block';
		    	}
			} else {
				outerMenu.style.visibility = 'visible';
				innerMenu.style.visibility = 'visible';
				if (content) {
					content.style.display = 'inline-block';
					logo.style.display = 'none';
		    	}
			}
		}
		
		showOrHideInteractiveMenu(); // TODO remove when interactive menu refactored
		
		/**
		 * Tmp listener
		 */
		window.onresize = function(event) {
			showOrHideInteractiveMenu();
		};
		
		// Create arrays of menu titles, prices and sandwiches
		var menuTitles = [], menuPrices = [], menuSandwiches = [];
		for (var idMenu in menuParams) {
			menuTitles.push(menuParams[idMenu]['title'].toUpperCase());
			menuPrices.push(menuParams[idMenu]['price']);
			var sandwiches = [];
			for (var idSandwich in menuParams[idMenu]['sandwiches']) {
				if (menuParams[idMenu]['sandwiches'][idSandwich]['ingredients'] && menuParams[idMenu]['sandwiches'][idSandwich]['price']) {
					sandwiches.push(menuParams[idMenu]['sandwiches'][idSandwich]);
				}
			}
			menuSandwiches.push(sandwiches);
		}
	    
	    var menuItems = sophien.createCircularMenu(document.getElementsByClassName('svg-container')[1], 250, 250, 250, 80, 360, menuTitles);
	    
	    for (var i = 0; i < menuItems.length; i++) {
	    	menuItems[i].addEventListener('click', menuClickHandler(menuPrices[i], menuSandwiches[i]));
	    }
	
	}
	
})();