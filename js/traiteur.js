jQuery(function($) {

	var total = 0;

	// Register event handler
	// Override input change handler from plugin for customization
	$(".addItemInput").change(addItemInputChangeCustomHandler);

	function addItemInputChangeCustomHandler() {
		var $row = $(this).closest(".row"); // Find the enclosing row
		var quantity = $(this).val();
		var price = $(this).data("price");

		// Always substract last row total from total
		var lastRowTotal = parseInt($row.find(".total-row").text()) || 0;
		total = total - lastRowTotal;

		if (!isNaN(quantity) && quantity > 0) {
			// Compute total for the row
			var newRowTotal = quantity * price;
			// Add it to total
			total = total + newRowTotal;
			// Set total for the row
			$row.find(".total-row").text(Utilities.formatPrice(newRowTotal, "€"));
		} else {
			$row.find(".total-row").text("");
		}

		renderTotal();
	}

	function renderTotal() {
		// Render total amount text
		if (total > 0) {
			$("#total").text(Utilities.formatPrice(total, "€"));
		} else {
			$("#total").text("");
		}
	}

});
