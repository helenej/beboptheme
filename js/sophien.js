// To set a style, overrides class "sophien-arc"
var sophien = (function() {
	var my = {};
	
	// Private variables
	var currentSelectedNode = null;

	// Public functions
	/**
	 * Params:
	 * 	centerX		Menu center (X)
	 * 	centerY		Menu center (Y)
	 * 	radius		Menu radius
	 * 	deltaRadius	Menu width
	 * 	itemCount	Number of menu items
	 * 	areaAngle	Total angle of the menu (degrees)
	 */
	my.createCircularMenu = function(element, centerX, centerY, radius, deltaRadius, areaAngle, items) {
		var itemCount = items.length;
		var angle = areaAngle / itemCount;

		var svg = SVG(element).size(500, 500).viewbox(0, 0, 500, 500);

		var paths = [];
		for (var i = 0; i < itemCount; i++) {
			var d = 'M '
					+ (centerX + radius)
					+ ','
					+ centerY
					+ ' A '
					+ radius
					+ ' '
					+ radius
					+ ' 0 0 0 '
					+ (centerX + (Math.cos(toRadians(angle)) * radius))
					+ ','
					+ (centerY - (Math.sin(toRadians(angle)) * radius))
					+ ' L '
					+ (centerX + (Math.cos(toRadians(angle)) * (radius - deltaRadius)))
					+ ','
					+ (centerY - (Math.sin(toRadians(angle)) * (radius - deltaRadius)))
					+ ' A ' + (radius - deltaRadius) + ' '
					+ (radius - deltaRadius) + ' 1 0 1 '
					+ (centerX + radius - deltaRadius) + ',' + centerY + ' Z';
			var path = svg.path(d).fill('none');
			path.addClass('sophien-arc');

			var rotate = ((areaAngle != 360) ? (-(angle * i) + ((areaAngle - 180) / 2))
					: (-(angle * i)));
			path.rotate(rotate, centerX, centerY);

			path.node.addEventListener('mouseover', function(event) {
				var classes = this.getAttribute('class');
				if (classes.indexOf('sophien-arc-hover') == -1) {
					classes += " sophien-arc-hover";
				}
				this.setAttribute('class', classes);
			});
			path.node.addEventListener('mouseout', function(event) {
				var classes = this.getAttribute('class');
				if (classes.indexOf('sophien-arc-hover') != -1) {
					classes = classes.replace(/\bsophien-arc-hover\b/, '');
				}
				this.setAttribute('class', classes);
			});

			path.node.addEventListener('click', function(event) {
				if (currentSelectedNode) {
					var classes = currentSelectedNode.getAttribute('class');
					if (classes.indexOf('sophien-arc-selected') == -1) {
						classes += " sophien-arc-selected";
					} else {
						classes = classes.replace(/\bsophien-arc-selected\b/,
								'');
					}
					currentSelectedNode.setAttribute('class', classes);
				}

				var classes = this.getAttribute('class');
				if (classes.indexOf('sophien-arc-selected') == -1) {
					classes += " sophien-arc-selected";
				} else {
					classes = classes.replace(/\bsophien-arc-selected\b/, '');
				}
				this.setAttribute('class', classes);

				currentSelectedNode = this;
			});
			
			/*path.mouseover(function(event) {
				if (!path.hasClass('sophien-arc-hover')) {
					path.addClass('sophien-arc-hover');
				}
			});
			path.mouseout(function(event) {
				if (!path.hasClass('sophien-arc-hover')) {
					path.removeClass('sophien-arc-hover');
				}
			});*/
	
			paths.push(path.node);

			// Text
			if ((i * angle) < 180) {
				d = 'M '
						+ (centerX + (Math.cos(toRadians(angle)) * (radius - (deltaRadius / 2))))
						+ ','
						+ (centerY - (Math.sin(toRadians(angle)) * (radius - (deltaRadius / 2))))
						+ ' A ' + (radius - (deltaRadius / 2)) + ' '
						+ (radius - (deltaRadius / 2)) + ' 0 0 1 '
						+ (centerX + radius - (deltaRadius / 2)) + ','
						+ centerY;
			} else {
				d = 'M '
						+ (centerX + radius - (deltaRadius / 2))
						+ ','
						+ centerY
						+ ' A '
						+ (radius - (deltaRadius / 2))
						+ ' '
						+ (radius - (deltaRadius / 2))
						+ ' 0 0 0 '
						+ (centerX + (Math.cos(toRadians(angle)) * (radius - (deltaRadius / 2))))
						+ ','
						+ (centerY - (Math.sin(toRadians(angle)) * (radius - (deltaRadius / 2))));
			}
			var text = svg.text(function(add) {
				add.tspan(items[i]);
			});
			text.path(d).font({
				family : 'Arial',
				anchor : 'middle'
			}).rotate(rotate, centerX, centerY);
			text.textPath().attr('startOffset', '50%');
	}
		return paths;
	};
	
	// Private function
	function toRadians(angle) {
		return angle * (Math.PI / 180);
	}
	
	return my;
})();