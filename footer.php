 <footer class="pal-small-pam-tiny-pas">
  <div class="grid-3-small-2-tiny-1">
   <div>
    <h3>Contactez-nous</h3>
    <p class="mll">
     <img src="<?php bloginfo('template_url'); ?>/images/icon_phone_small.png" alt="Téléphone" /><a href="tel:0561123206" class="mls">05 61 12 32 06</a>
    </p>
    <p class="mll">
     <img src="<?php bloginfo('template_url'); ?>/images/icon_mail_small.png" alt="E-mail" /><a href="mailto:bebop.toulouse@gmail.com?Subject=Site%20web" target="_top" class="mls">bebop.toulouse<wbr>@gmail.com</a>
    </p>
   </div>
   <div>
    <h3>Retrouvez-nous</h3>
    <p class="mll">
     <img src="<?php bloginfo('template_url'); ?>/images/icon_marker_small.png" alt="Adresse" /><span class="mls">45, rue de Metz, Toulouse</span>
    </p>
    <p class="mll">
     <img src="<?php bloginfo('template_url'); ?>/images/icon_clock_small.png" alt="Horaire" /><span  class="mls">Lun. - Ven. : 7h30 - 19h30</span>
    </p>
   </div>
   <div>
    <h3>Suivez-nous</h3>
    <p class="mll">
     <img src="<?php bloginfo('template_url'); ?>/images/icon_facebook_small.png" alt="Facebook" /><a href="http://www.facebook.com/bebopcafe" target="_blank"  class="mls">www.facebook.com<wbr>/bebopcafe</a>
    </p>
   </div>
  </div>
  
  <?php get_sidebar(); ?>
  
  <div class="ptl copyright">Tous droits réservés - <?php echo date("Y"); ?> - <span class="txt-green">Bebop café</span></div>
  
  <?php wp_footer(); ?>
 </footer>
</div> <!-- end of main_wrapper -->
</body>
</html>
